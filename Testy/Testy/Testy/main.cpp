//
//  main.cpp
//  Testy
//
//  Created by Samuel Inniss on 8/28/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#include <iostream>
#include "testytest.h"

using namespace std;

int main()
{
    int first, second, third;
    testytest A;
    
    cout << "How many numbers?\nEnter 1, 2, or 3\n";
    cin >> first;
    
    switch(first)
    {
    case 1:
        cout << "Kay, enter your number\n";
        cin >> first;
        A.setNum(first);
        cout << "Your answer is " << A.fishy();
        break;
        
    case 2:
        cout << "Kay, enter your numbers\n";
        cin >> first >> second;
        A.setNum(first,second);
        cout << "Your answer is " << A.fishy();
        break;
        
    case 3:
        cout << "Kay, enter your numbers\n";
        cin >> first >> second >> third;
        A.setNum(first,second,third);
        cout << "Your answer is " << A.fishy();
        break;
        
    default:
        cout << "Invalid number of numbers!\n";
    }
    
    
    
    return 0;
}

